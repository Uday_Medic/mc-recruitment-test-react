# Medic Creations recruitment test - React.js Instructions

Thanks for taking the time to do our front-end / full-stack coding test. The challenge has two parts:

- a task to create a basic app for educating healthcare professionals how to put on and take off personal protective equipment (PPE) 
- some follow-up questions

You will be graded based on the following have been met:

- Your implementation works as described in the task.

## Task
### Approach
Create a React web app that will:

- Persist the data from an API call in redux and store locally allowing user to use it as a progressive web app
- Have a contact form with validations that will be sent to Netlify forms

The base project is available in this repo and we want you to fork this repo.

### Task requirements
Feel free to spend as much or as little time on the exercise as you like as long as the following requirements have been met:

- Please complete the user stories below.
- Your code should run in one step.
- We'd like you to use React. On top of that, use whatever front-end libraries you feel comfortable with.
- You must include tests

### User Stories
As a user I want to easily find the right PPE information I need

As a user I want to use the PPE information offline on my phone

As a user I want to send a contact form submission to Medic Bleep

## Design
Design assets are available as a Sketch file via Sketch Cloud - https://www.sketch.com/s/PbxpL

We've provided a design for small-screens (480px). Don't worry about tackling larger breakpoints, but please make sure your solution looks good at 480px in portrait orientation.

Please do not spend a lot of time on styling as this is not the key part of the assessment, and you will not be penalised for basic styles. For any styling that you do, we would like to see you using styled components or emotion.

## API
The content will come from a single API endpoint. Which can be found at https://getshippin.com/api/apps/14/items

The endpoint requires an API Key: shippin_DzYCDD_PFJe-p3Wbml5e9g

Example curl request:

```curl -X GET  -H "accept:application/json" -H "content-type:application/json" -H "authorization:Bearer shippin_DzYCDD_PFJe-p3Wbml5e9g" https://getshippin.com/api/apps/14/items```

The data has the structure:

- PPE Guide
    - title: string
    - description: text
    - short_description: string
    - video_url: string
    - steps: array
        - content: string
        - order: string
- PPE Item
    - name: string
    - description: text


## Client implementation
We'd like you to use React. 
Use a tool to format code (eg prettier).
Use Redux for state with the use of redux persist, redux saga and redux toolkit as necessary.
Use React conventions such as reusable components and new lifecycle methods.
On top of that, use whatever front-end libraries you feel comfortable with.

## Submission Guidelines
Fork this repo and send a link to the repo on Github/bitbucket/gitlab via email back to your contact.
The FOLLOW-UP.md file should have answers to the follow-up questions.

---

Inspiration for the test format taken with ❤️ from JustEat's recruitment test and Skyscanner recruitment test.

---

# PPE Guide Web App - Development Readme

react repo for the Medic Bleep PPE Guide App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Development

## Running the app locally

#TODO

## Structure

We're using `redux` for state management and `redux-saga` for asynchronous actions e.g. api requests.

The bulk of the code is in the `app` directory.

| location       | contents                                                                                                                     |
| -------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| src/App.js     | Entrypoint for the app                                                                                                       |
| src/api        | API interaction                                                                                                              |
| src/components | lower level components, e.g. buttons                                                                                         |
| src/screens    | components representing entire screens within the app, where integration with redux would happen                             |
| src/config     | app-wide config - things like an api host, colors, etc. Configuration of the redux store and, in dev, tools like Reactotron. |
| src/state      | redux reducers/actions/selectors. Combined in `index.js`                                                                     |
| src/sagas      | `redux-saga` sagas, forked from the root saga in `index.js` to run in parallel.                                              |

## Config

Global app config is in `app/config/index.js`. There are some defaults which are overridden by the contents of `local.js` and either `development.js` or `production.js`, in that order, depending on if the app is being run locally or built as a release.

`local.js` is intended for overriding config values without committing them. Things like enabling/disabling storybook locally or secret tokens.

`production.js` is applied last, so local values for these won't have an effect.

#### Possible config values

**TODO** Update these as config values are established

| value     | purpose                            |
| --------- | ---------------------------------- |
| `colors`  | The colors used throughout the app |
| `apiKey`  | apiKey used for fetching app data  |
| `baseUrl` | url for api calls                  |

## Redux

**TODO** Setup redux

Our typical redux setup would be done in `config/store.js`. This would setup the redux store, add middleware (such as redux-saga and redux-persist).

We would then add the react-redux `Provider` to `app/App.js`.

We have included `@reduxjs/toolkit` which speeds up development by allowing us to abstract away most of the typical boilerplate code associated with setting up and using redux. For example:

- Includes a convenience function for configuring the store
- Has the concept of a `slice` which incorporates reducers and action creators

It's worth reading through the [toolkit docs](https://redux-toolkit.js.org/) for more details

## Dev Tools

**TODO** Implement

Add a suitable suggested develompent tool or tools. List what you use and why you use it.

## Tests

Run the tests with `yarn test`. We use a mixture of snapshot tests for making sure we don't make any unintentional changes to components or redux state mutations, and unit tests elsewhere.

## Code style

We use `prettier` for code formatting.

# Build & Deploy

**TODO** document how to build and deploy
